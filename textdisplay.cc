#include "textdisplay.h"
#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

textDisplay::textDisplay(){
  squares[0] = "COLLECT OSAP";
  squares[1] = "AL";
  squares[2] = "SLC";
  squares[3] = "ML";
  squares[4] = "TUITION";
  squares[5] = "MKV";
  squares[6] = "ECH";
  squares[7] = "NEEDLES HALL";
  squares[8] = "PAS";
  squares[9] = "HH";
  squares[10] = "DC TIMS LINE";
  squares[11] = "RCH";
  squares[12] = "PAC";
  squares[13] = "DWE";
  squares[14] = "CPH";
  squares[15] = "UWP";
  squares[16] = "LHI";
  squares[17] = "SLC";
  squares[18] = "BMH";
  squares[19] = "OPT";
  squares[20] = "GOOSE NESTING";
  squares[21] = "EV1";
  squares[22] = "NEEDLES HALL";
  squares[23] = "EV2";
  squares[24] = "EV3";
  squares[25] = "V1";
  squares[26] = "PHYS";
  squares[27] = "B1";
  squares[28] = "CIF";
  squares[29] = "B2";
  squares[30] = "GO TO TIMS";
  squares[31] = "EIT";
  squares[32] = "ESC";
  squares[33] = "SLC";
  squares[34] = "C2";
  squares[35] = "REV";
  squares[36] = "NEEDLES HALL";
  squares[37] = "MC";
  squares[38] = "COOP FEE";
  squares[39] = "DC";
  for (int i = 0; i < 40; i++){
    for(int j = 0; j < 8; i++){
      playerPos[i][j] = ' ';
    }
  }
  for (int i = 0; i < 40; i++){
    improv[i] = 0;
  }
}

textDisplay::~textDisplay(){}

void textDisplay::notifyJoin(char newplayer){
  if(player == 'G') { playerPos[0][0] = newplayer; }
  else if (player == 'B') { playerPos[0][1] = newplayer; }
  else if (player == 'D') { playerPos[0][2] = newplayer; }
  else if (player == 'P') { playerPos[0][3] = newplayer; }
  else if (player == 'S') { playerPos[0][4] = newplayer; }
  else if (player == '$') { playerPos[0][5] = newplayer; }
  else if (player == 'L') { playerPos[0][6] = newplayer; }
  else if (player == 'T') { playerPos[0][7] = newplayer; }
}

void textDisplay::notifyMove(char player, const int pos, const int dis){
  int player_ind;
  int new_pos = pos + dis;
  if(player == 'G') { player_ind = 0; }
  else if (player == 'B') { player_ind = 1; }
  else if (player == 'D') { player_ind = 2; }
  else if (player == 'P') { player_ind = 3; }
  else if (player == 'S') { player_ind = 4; }
  else if (player == '$') { player_ind = 5; }
  else if (player == 'L') { player_ind = 6; }
  else if (player == 'T') { player_ind = 7; }
  playerPos[pos][player_ind] = ' ';
  playerPos[pos + dis][player_ind] = player;
}

void textDisplay::notifyImprov(int pos, bool action){ // ture is buy. false is sell
  if(action) ++improv[pos];
  else --improv[pos];
}

void textDisplay::print() const{
  cout.fill('_');
  cout.width(111);
  cout << endl;
  for(int i = 20; i <= 30; i++){
    
  }
}
