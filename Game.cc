#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "building.h"
#include "Game.h"
#include "player.h"
#include "collectosap.h"
#include "al.h"
#include "slc.h"
#include "ml.h"
#include "tuition.h"
#include "mkv.h"
#include "ech.h"
#include "pas.h"
#include "needleshall.h"
#include "phys.h"
#include "b1.h"
#include "hh.h"
#include "dctimsline.h"
#include "rch.h"
#include "pac.h"
#include "dwe.h"
#include "cph.h"
#include "uwp.h"
#include "lhi.h"
#include "bmh.h"
#include "opt.h"
#include "goosenesting.h"
#include "ev1.h"
#include "ev2.h"
#include "ev3.h"
#include "v1.h"
#include "cif.h"
#include "b2.h"
#include "gototims.h"
#include "eit.h"
#include "esc.h"
#include "c2.h"
#include "rev.h"
#include "mc.h"
#include "dc.h"
#include "coopfee.h"

using namespace std;

Game::Game(){
	Board[0] = new collectOsap();
	Board[1] = new Al();
	Board[2] = new slc();
	Board[3] = new Ml();
	Board[4] = new tuition();
	Board[5] = new Mkv();
	Board[6] = new Ech();
	Board[7] = new needlesHall();
	Board[8] = new Pas();
	Board[9] = new Hh();
	Board[10] = new dcTimsLine();
	Board[11] = new Rch();
	Board[12] = new Pac();
	Board[13] = new Dwe();
	Board[14] = new Cph();
	Board[15] = new Uwp();
	Board[16] = new Lhi();
	Board[17] = new slc();
	Board[18] = new Bmh();
	Board[19] = new Opt();
	Board[20] = new gooseNesting();
	Board[21] = new Ev1();
	Board[22] = new needlesHall();
	Board[23] = new Ev2();
	Board[24] = new Ev3();
	Board[25] = new V1();
	Board[26] = new Phys();
	Board[27] = new B1();
	Board[28] = new cif();
	Board[29] = new B2();
	Board[30] = new goToTims();
	Board[31] = new Eit();
	Board[32] = new Esc();
	Board[33] = new slc();
	Board[34] = new C2();
	Board[35] = new Rev();
	Board[36] = new needlesHall();
	Board[37] = new Mc();
	Board[38] = new coopFee();
	Board[39] = new Dc();
	dice1 = 0;
	dice2 = 0;
	bank = new Bank(this);
	PlayerNumber = 0;
	TotalPlayer = 0;
	Display = new TextDisplay();
	for(int i = 0; i < 8; i++){
		players[i] = NULL;
	}
	DoubleCount=0;
	OriPlayerNum=0;
	ifdiced = false;
};

Game::~Game(){

}

void Game::LoadFilePlay(){
	string file,line;
	cout << "What's the name of file" << endl;
	cin >> file;
	ifstream lf;
	lf.open(file);
	getline(lf,line);
	istringstream(line) >> OriPlayerNum;
	for(int i = 0; i < OriPlayerNum; i++){
		getline(lf,line);
		string player,chara;
		int cups, money, pos;
		bool IfDc = false;
		int IfInDc = 0,TimeInDc = 0;
		istringstream(line) >> player >> chara >> cups >> money >> pos;
		if (pos == 10) {
			istringstream(line) >> IfInDc >> TimeInDc;
		}
		if(IfInDc == 0){
			IfDc = false;
		}
		else if (IfInDc == 1){
			IfDc = true;
		}
		Player* temp = NULL;
		char c = GetChar(chara);
		int i = TotalPlayer;
		players[i] = new Player(this,player,c,cups,money,pos,IfDc,TimeInDc);
		TotalPlayer++;
		Display->notifyJoin(c);
		Display->notifyMove(player,0,pos);
	}
	while(getline(lf,line)){
		string building,owner;
		int improvements;
		istringstream(line) >> building >> owner >> improvements;
		int ind = GetProInd(building);
		char PlayerChar;
		for (int i = 0; i < OriPlayerNum; i++){
			if (players[i]->GetName() == owner){
				players[i]->AddProperty(ind);
				PlayerChar = players[i]->GetChar();
			}
		}
		Board[ind]->setOwner(PlayerChar);
		Board[ind]->setImproved(improvements);
		if(improvements < 0){
			Display->notifyImprov(ind,false);
		}
		else{
			Display->notifyImprov(ind,true);
		}
	}
	int i = 0;
	while (i < OriPlayerNum){
		if (!players[i]->IsBankrupt()){
			PlayerNumber = i;
			if (players[i]->GetTimLine){
				dctimline::arrive();
			}
			else{
				string name = player[i]->GetName();
				cout << name << ":" << endl;
				cout << "please roll dice, type 'roll'" << endl;
				continu();
			}
		}
		i++;
		if (i == 8){
			i = 0;
		}
	}
}

void Game::NewGamePlay(){
	cout << "How many players?" << endl;
	cout << "number must between 6-8" << endl;
	int NumPlay;
	cin >> NumPlay;
	while(NumPlay < 6 || NumPlay > 8) {
		cout << "re-enter the number" << endl;
		cin >> NumPlay;
	}
	OriPlayerNum = NumPlay;

	for (int i = 0; i < OriPlayerNum; i++){
		string playername,playerchar;
		cout << "please enter your name" << endl;
		cin >> playername;
		cout << "please enter your charactor" << endl;
		cin >> playerchar;
		bool goose,grt,thd,pro,stu,money,laptop,pink = false;
		if (playerchar == "Goose") {
			goose = true;
		}
		if (playerchar == "GRT Bus") {
			grt = true;
		}
		if (playerchar == "Tim Hortons Doughnut"){
			thd = true;
		}
		if (playerchar == "Professor"){
			pro = true;
		}
		if (playerchar == "Student"){
			stu = true;
		}
		if (playerchar == "Money"){
			money = true;
		}
		if (playerchar == "Laptop"){
			laptop = true;
		}
		if (playerchar == "Pink tie"){
			pink = true;
		}
		while ((playerchar != "Goose" &&
				playerchar != "GRT Bus" &&
				playerchar != "Tim Hortons Doughnut" &&
				playerchar != "Professor" &&
				playerchar != "Student" &&
				playerchar != "Money" &&
				playerchar != "Laptop" &&
				playerchar != "Pink tie" )||
				((playerchar == "Goose"&& goose) ||
				(playerchar == "GRT Bus" && grt) ||
				(playerchar == "Tim Hortons Doughnut" && thd) ||
				(playerchar == "Professor" && pro) ||
				(playerchar == "Student" && stu) ||
				(playerchar == "Money" && money) ||
				(playerchar == "Laptop" && laptop) ||
				(playerchar == "Pink tie" && pink))){
			cout << "please enter valid charactor" << endl;
			cin >> playerchar;
		}
		char a = GetChar(playerchar);
		int i = TotalPlayer;
		players[i] = new Player(this,playername,a,0,1500,0,false,0);
		TotalPlayer++;
		Display->notifyJoin(a);
	}
	Display->print();

	cout << "Done Registing" << endl;
	cout << endl << "GAME START" << endl;

	int i = 0;
	while (i < OriPlayerNum){
		if (!players[i]->IsBankrupt()){
			if (players[i]->GetTimLine){
				dctimline::arrive();
			}
			else{
				PlayerNumber = i;
				string name = player[i]->GetName();
				cout << name << ":" << endl;
				cout << "please roll dice, type 'roll'" << endl;
				continu();
			}
		}
		i++;
		if(i == 8){
			i = 0;
		}
	}
}

void Game::continu(){
	string command ="";
	while (command != "save"|| TotalPlayer !=1){
		cin >> command;


		if(command == "roll"){
			if (ifdiced) {
				cout << "you have already rolled" <<endl;
				cout << "please enter other command" << endl;
			}
			else{
				Roll();
				cout << "you rolled " << dice1 << " and " << dice2 << endl;
				ifdiced = true;
				bool doubles = CheckDoubles();
				if (doubles){
					DoubleCount++;
					if (DoubleCount != 3){
						ifdiced = false;
						cout << "you rolled double dice" << endl;
						cout << "please roll again" << endl;
					}
					else if (DoubleCount == 3){
						cout << "you have to go to DC Tims Line" << endl;
						dctimsline::arrive(players[PlayerNumber]);
						int dis;
						if (players[PlayerNumber] > 10){
							dis = 39 - players[PlayerNumber] + 11;
						}
						else{
							dis = 10 - players[PlayerNumber];
						}
						Display->notifyMove(players[PlayerNumber]->GetChar(),players[PlayerNumber]->GetPos,dis);
						Display->print;
						DoubleCount = 0;
					}
				}
				else{
					players[PlayerNumber]->move(dice1+dice2);
					Display->notifyMove(players[PlayerNumber]->GetChar(),players[PlayerNumber]->GetPos,dice1+dice2);
					int pos = players[PlayerNumber]->GetPos();
					Building* tembuid = Getproperty(pos+dice1+dice2);
					tembuid->arrive();
					DoubleCount = 0;
				}
			}
		}


		else if(command == "next"){
			if(ifdiced){
				break;
			}
			else{
				cout << "please roll your dice" << endl;
			}
		}


		else if(command == "trade"){
			string name,pro1,pro2,ifagree;
			int money;
			cin >> name;
			int ind = GetPlayInd(name);
			if (cin >> money){
				cin >> pro1;
				cout << players[PlayerNumber]->GetName();
				cout << " want to exchange " << money << " with ";
				cout << name << "'s " <<pro1 << endl;
				cout << "does " <<name << " accept?" << endl;
				cout << "if yes type 'yes', if no type 'no'" << endl;
				cin >> ifagree;
				if (ifagree == "yes"){
					trade1(ind,money,pro1);
				}
			}
			else if (cin >> pro1){
				if (cin >> money){
					cout << players[PlayerNumber]->GetName();
					cout << " want to exchange " << pro1 << " with ";
					cout << name << "'s " << money << endl;
					cout << "does " <<name << " accept?" << endl;
					cout << "if yes type 'yes', if no type 'no'" << endl;
					cin >> ifagree;
					if (ifagree == "yes"){
						trade3(ind,pro1,money);
					}
				}
				else if (cin >> pro2){
					cout << players[PlayerNumber]->GetName();
					cout << " want to exchange " << pro1 << " with ";
					cout << name << "'s " << pro2 << endl;
					cout << "does " <<name << " accept?" << endl;
					cout << "if yes type 'yes', if no type 'no'" << endl;
					cin >> ifagree;
					if (ifagree == "yes"){
						trade2(ind,pro1,pro2);
					}
				}
			}
		}


		else if(command == "improve"){
			cout << "enter the property you want to improve" << endl;
			cout << "enter 'buy' or 'sell' for purpose" << endl;
			string pro,pur;
			cin >> pro >> pur;
			improve(pro,pur);
		}


		else if(command == "mortgage"){
			string pro;
			cout << "enter the property you want to mortgage" << endl;
			cin >> pro;
			int proind = GetProInd(pro);
			Board[proind]->mortgage();
		}
		else if(command == "unmortgage"){
			string pro;
			cout << "enter the property you want to unmortgage" << endl;
			cin >> pro;
			int proind = GetProInd(pro);
			Board[proind]->unmortgage();
		}

		// need to change***********************************************
		else if(command == "bankrupt"){
			players[PlayerNumber]->SetBankrupt();
		}
		//************************************************************

		else if(command == "assets"){
			int money = players[PlayerNumber]->ShowMoney();
			cout << "now, you have " << money << " dollars" << endl;
		}
		else if(command == "all"){
			AllAssets();
		}
		else if(command == "save"){
			Save();
		}
		else if(command == "-testing"){

		}
		else{
			cout << "please enter the command again " << endl;
			cout << "the available commands are:" << endl;
			cout << "----------[roll]------[next]------[trade]------"
					"[improve]------[mortgage]------[unmortgage]"
					"------[bankrupt]------[assets]------[all]------"
					"[save]----------" << endl;
		}
	}
	if (TotalPlayer == 1){
		string name;
		for (int i = 0; i < OriPlayerNum; i++){
			if (!players[i]->IsBankrupt()){
				name = players[i]->GetName();
			}
		}
		cout << "Game ends" << endl;
		cout << "the winner is " << name << endl;
	}
}

void Game::Test(){
}

void Game::SetNumplay(int n){
	OriPlayerNum = n;
}

bool Game::CheckDoubles(){
	if (dice1 == dice2){
		return true;
	}
	else{
		return false;
	}
}

void Game::Roll(){
	unsigned int dic1;
	unsigned int dic2;
	dic1 = std::rand() % 6 + 1;
	dic2 = std::rand() % 6 + 1;
	dice1 = dic1;
	dice2 = dic2;
}


void Game::trade1(int ind, int money, string property){
	players[PlayerNumber]->LoseMoney(money);
	int proind = GetProInd(property);
	players[PlayerNumber]->GetProperty(proind);
	players[ind]->GetMoney(money);
	players[ind]->LoseProperty(proind);
}

void Game::trade2(int ind, string pro1, string pro2){
	int pro1ind = GetProInd(pro1);
	int pro2ind = GetProInd(pro2);
	players[PlayerNumber]->LoseProperty(pro1ind);
	players[PlayerNumber]->GetProperty(pro2ind);
	players[ind]->GetProperty(pro1ind);
	players[ind]->LoseProperty(pro2ind);
}

void Game::trade3(int ind, string pro, int money){
	int proind = GetProInd(pro);
	players[PlayerNumber]->LoseProperty(proind);
	players[PlayerNumber]->GetMoney(money);
	players[ind]->LoseMoney(money);
	players[ind]->GetProperty(proind);
}

void Game::improve(string pro, string pur){
	int ind = GetProInd(pro);
	if (pur == "buy"){
		if (players[PlayerNumber]->Monopoly(Board[ind])){
			Board[ind]->BuyImprove();
			Display->notifyImprov(ind,true);
		}
		else{
			Display->notifyImprov(ind,false);
			cout << "you didn't buy all the properties in the monopoly "
					<< "you cannot buy improvement of this building"
		}
	}
	else if (pur == "sell"){
		Board[ind]->SellImprove();
	}
}


void Game::AllAssets(){
	for (int i = 0; i < OriPlayerNum; i++){
		string name = players[i]->GetName();
		int money = players[i]->ShowMoney();
		if (!players[i]->IsBankrupt()){
			cout << name << " have " << money << " dollar." << endl;
		}
		else {
			cout << name << " is brukrupted" << endl;
		}
	}
}


void Game::Save(){
	cout << "enter the filename you want to save as:"<< endl;
	string file;
	cin >> file;
	ofstream output(file);
	output << TotalPlayer << endl;

	for(int i = 0; i < OriPlayerNum; i++){
		if (!players[i]->IsBankrupt()){
			string name;
			char chara;
			bool intim;
			int timcups,money,pos,ifintim,numintim;
			name = players[i]->GetName();
			chara = players[i]->GetChar();
			timcups = players[i]->GetTimcups();
			money = players[i]->ShowMoney();
			pos = players[i]->GetPos();
			intim = players[i]->TimLine();
			numintim = players[i]->GetTimDice();
			if (intim){
				ifintim = 1;
			}
			else if (!intim){
				ifintim = 0;
			}
			if (pos == 10){
				if (ifintim == 1){
					output << name << " "
							<< chara << " "
							<< timcups << " "
							<< money << " "
							<< pos << " "
							<< ifintim << " "
							<< numintim << " " <<endl;
				}
				else if (ifintim == 0){
					output << name << " "
							<< chara << " "
							<< timcups << " "
							<< money << " "
							<< pos << " "
							<< ifintim << " " << endl;
				}
			}
			else{
				output << name << " "
						<< chara << " "
						<< timcups << " "
						<< money << " "
						<< pos << " " << endl;
			}
		}
	}
	for(int i = 0; i < 40; i++){
		string name,owner;
		int improv;
		name = Board[i]->getName();
		owner = Board[i]->getOwner();
		improv = Board[i]->getImproved();
		output << name << " " << owner << " " << improv;
	}
	output.close();
	cout << "Done saving" <<endl;
}


char Game::GetChar(string chara){
	if (chara == "Goose"){
		return "G";
	}
	else if (chara == "GRT Bus"){
		return "B";
	}
	else if (chara == "Tim Hortons Doughnut"){
		return "D";
	}
	else if (chara == "Professor"){
		return "P";
	}
	else if (chara == "Student"){
		return "S";
	}
	else if (chara == "Money"){
		return "$";
	}
	else if (chara == "Laptop"){
		return "L";
	}
	else if (chara == "Pink tie"){
		return "T";
	}
}

int Game::GetPlayInd(string name){
	for (int i = 0; i < OriPlayerNum; i++){
		if (players[i]->GetName() == name){
			return i;
		}
	}
}

int Game::GetProInd(string name){
	for(int i = 0; i < 40; i++){
		if (Board[i]->getName() == name){
			return i;
		}
	}
}

Player* Game::Getplayer(int i){
	return players[i];
}

Building* Game::Getproperty(int i){
	return Board[i];
}


void Game::AddPlayer(Player* tem){
	int i = TotalPlayer;
	players[i] = tem;
	TotalPlayer++;
}








